from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class Tag(Base):

    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True)
    tagid = Column(String, nullable=False)