import serial
import time, datetime, sys
from sqlalchemy import create_engine, exists
from sqlalchemy.orm import sessionmaker
from models import Tag
from misc import Helpers
import requests

if __name__ == '__main__':
    push_url = 'http://{}/trackerrfid/trial.php'.format(sys.argv[2])
    scan_url = 'http://{}/trackerrfid/scan.php'.format(sys.argv[2])
    print(push_url); print(scan_url)
    
    print("Com port: {}".format(sys.argv[3]))
    with serial.Serial('{}'.format(sys.argv[3]), 9600, timeout=1) as ser:
        engine = create_engine("mysql://test:test@localhost/test")
        connection = engine.connect()
        Session = sessionmaker(bind=engine)
        session = Session()
        tag_id = ''
        
        START_BYTE = b'\x02'
        END_BYTE = b'\x03'
        reading_tag = False
        while True:
            serial_line = ser.read()
            if serial_line not in [b'\xff', b'', b'\n']:
                if serial_line == START_BYTE:
                    reading_tag = True
                    print("Started reading tag")
                elif serial_line == END_BYTE:
                    reading_tag = False
                    print(tag_id)
                    print("Stopped reading tag")

                    if sys.argv[1] == 'scan':
                        print("Scanning mode")
                        r = requests.get(scan_url, params={"addr": tag_id})
                        print(r.text)
                        print(tag_id)
                    else:
                        t = Tag()
                        t.tagid = tag_id
                        session.add(t)
                        session.commit()
                        position = Helpers.rand_loc()
                        print(position)
                        r = requests.get(push_url,
                            params={
                                "addr": tag_id,
                                "reported": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                "xpos": position[0],
                                "ypos": position[1]})
                        print(r.text)
                        print(tag_id)
                    tag_id = ''                 
                else:
                    print(serial_line)
                    tag_id += serial_line.hex()
            else:
                print("Bad data"); print(serial_line)
            time.sleep(0.1)
        connection.close()