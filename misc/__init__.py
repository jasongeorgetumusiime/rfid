import random

class Helpers(object):
    tag_group = {
        '2A0AE6AF414E10110000000000' : 6,
        'EA0AE6AF414E10110000000000t': 4,
        '8D93E86CA14830110000000000': 5,
        '7893E86CA14830110000000000r': 2
    }

    positions = [
        [130, 100], 
        [310, 140], 
        [480, 150], 
        [640, 160], 
        [130, 340], 
        [420, 300], 
        [640, 260], 
        [650, 410]
    ]

    @staticmethod
    def sanitize_data(self, data):
        """
        Clean the data and return list of useful information
        :param data: string representing serial data from an RFID reader
        :return tag_ids: a list of RFID tag ids or None     
        """
        
        pass

    @staticmethod
    def get_tag_id(name):
        return Helpers.tag_group[name]

    @staticmethod
    def rand_loc():
        return Helpers.positions[random.randint(0, len(Helpers.positions) - 1)]